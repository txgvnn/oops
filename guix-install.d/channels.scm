(list (channel
       (name 'guix)
       (branch "master")
       (commit "5d10644371abd54d0edcd638691113f0a92de743")
       (url "https://git.savannah.gnu.org/git/guix.git"))
      (channel
       (name 'guxti)
       (branch "master")
       (commit "1bc8b62fe4d2dd9c86f946ffae1401bf7f8bae00")
       (url "https://github.com/TxGVNN/guxti")))
